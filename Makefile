
MAKEFILENAME := $(lastword $(MAKEFILE_LIST))
MAKEDIR      := $(shell dirname $(MAKEFILENAME) | xargs readlink -f)

DOCS ?= $(shell find documents -mindepth 1 -maxdepth 1 -type d 2> /dev/null)
DEP  := $(DOCS:%=%/main.dep)

INSTALLROOT ?= $(MAKEDIR)/docs

VERBOSE := 
ifdef VERBOSE
        VERBOSE_OPT := -v
endif

ASCIIDOC := asciidoc
A2X      := a2x

STYLE_OPT := -a stylesdir=style/css -a icons -a iconsdir=style/icons -a scriptsdir=style/js -a latexmath -a numbered -a linkcss
ASCIIDOC_OPT := -f $(MAKEDIR)/style/conf/pvasciidoc.conf

A2X_OPT := $(VERBOSE_OPT) $(STYLE_OPT)
A2X_OPT += --asciidoc-opts="$(ASCIIDOC_OPT)"
A2X_OPT += -r $(MAKEDIR)/style/css -r $(MAKEDIR)/style/js
A2X_OPT += --stylesheet="style/css/docbook-xsl.css style/css/inendi.css style/css/toc2.css"

#SINGLE_OPT   := $(A2X_OPT) -f xhtml -a toc -a max-width=100%
SINGLE_OPT   := $(VERBOSE_OPT) $(ASCIIDOC_OPT) -b html $(STYLE_OPT) -a stylesheet=inendi.css -a toc2 -a max-width=100%
CHUNKED_OPT  := $(A2X_OPT) -f chunked
PDF_OPT      := -f pdf $(A2X_OPT)     --fop --xsl-file style/css/pvfo.xsl


WATERMARK_JS_FILE := "js/.watermark.js"

ifeq ($(THISMAKEFILEOPTION),compile)

.PHONY : all single chunked pdf

all : single pdf

single ::

chunked ::

pdf ::


.PHONY : single_wm pdf_wm

single_wm ::

pdf_wm ::


.PHONY: install single_install pdf_install

install : single_install pdf_install

single_install :: $(INSTALLROOT)

pdf_install :: $(INSTALLROOT)

$(INSTALLROOT) :
	@mkdir -p $(INSTALLROOT)

include $(DEP)

else

.PHONY : all single chunked pdf

all : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@

single : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@

chunked : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@

pdf : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@


.PHONY : watermark single_wm pdf_wm

watermark : single_wm pdf_wm

single_wm : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@

pdf_wm : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@


.PHONY: install single_install pdf_install

install : single_install pdf_install

single_install : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@

pdf_install : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $@


# special target to run a single outputs/install target
%.pdf : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $(MAKEDIR)/$@

%/index.html : dep FORCE
	@THISMAKEFILEOPTION=compile make --no-print-directory -f $(MAKEFILENAME) $(MAKEDIR)/$@


.PHONY : dep

dep : $(DEP) ;

%.dep : %.txt
	@echo " DEP     $@"
	@scripts/mkdep-asciidoc.sh $< > $@


.PHONY : clean distclean

clean :
	@echo " CLEAN   noise files"
	@find . -name "*~" -o -name "\#*" -o -name "*.rej" -o -name "*.orig" | xargs rm -f

distclean : clean
	@echo " CLEAN   all dependencies files"
	@rm -f $(DEP)
	@echo " CLEAN   all outputs"
	@rm -rf outputs/*


.PHONY : again

again :: distclean all


.PHONY : c dc a

# RH: short target I like :p
c :: clean

dc :: distclean

a :: again

endif

FORCE : ;
