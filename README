
##############################################################################
# Présentation

L'ensemble de la documentation de INENDI est écrite au format ASCIIDOC et est
produite sous 3 formes différentes :

* page HTML unique (version single)
* une page HTML par section (version chunked)
* fichier PDF (version pdf)

La première forme est générée via la commande 'asciidoc' tandis que les 2
autres sont générées via la commande 'a2x'. À terme, la version single sera
aussi générée via 'a2x', une fois que le paramétrage de 'a2x' aura été
compris...

La version chunked n'est en réalité pas distribuée ; cela pourra changer.


##############################################################################
# Génération de la documentation

Un Makefile générique avec un mécanisme automatique de génération des
dépendances se charge de la production des 3 formes de la documentation.

Le Makefile fournit un ensemble réduit de cibles pour la génération de la
doc :

* all : génère toutes les versions pour tous les documents
* single : génère la version single pour chaque document
* chunked : génère la version chunked pour chaque document
* pdf : génère la version PDF pour chaque document

Afin de ne traiter qu'un seul document (ou un sous ensemble de toute la
documentation), la variable d'environnement "DOCS" peut être surchargée
pour faire cette restriction.

$ make pdf DOCS=documents/inendi_inspector_reference_manual

permet de ne générer que la version PDF du manuel de réf de PV-I


Pour la diffusion de la documentation avec filigrane, 3 cibles sont
utilisables :

* watermark : génère les versions single et pdf avec filigrane pour chaque
  document
* single_wm : génère la version single avec filigrane pour chaque document
* pdf_wm : génère la version PDF avec filigrane pour chaque document

À noter que les variables d'environnement COMPANY_NAME et CONTACT_NAME doivent
être définies.


Afin d'installer la documentation dans un répertoires unique (que ce soit sur
le site web des clients ou dans l'arborescence de PV-I), 3 cibles sont
définies :

* install: installe les versions single et pdf avec filigrane pour chaque
  document
* single_install : installe la version single avec watermarking pour chaque
  document
* pdf_install : installe la version PDF avec watermarking pour chaque
  document

Le répertoire destination est definie via la variable d'environnement DESTDIR
(avec "docs" comme valeur implicite)


pour les bourrins (comme moi (RH)), on peut aussi faire :

$ make docs/inendi_station_administration_guide.pdf

qui produira le PDF avec filigrane du guide d'admin des ASPI.


##############################################################################
# Structure système de la documentation

Afin d'avoir un style cohérent, les fichiers nécessaires au style INENDI
(HTML comme PDF) sont placés dans le répertoire "style/".


Chaque document est centralisé dans un répertoire situé dans "documents/" et
doit porter le nom utilisé lors de sa distribution.

Chaque document est structuré de la manière suivante :

* un fichier "main.txt" à la racine servant de document maître
* un dossier "images" avec les éventuelles images du documents
* des éventuels sous-répertoires si le document est scindé en plusieurs
  fichiers.


##############################################################################
# Syntaxe

$ asciidoc -h syntax

est votre amis

Pour l'indentation des sections, on utilise celle donnée en exemple par cette
commande.