
Concepts in brief
-----------------

This chapter presents, in a rather concise way, all the concepts that are at the core of INENDI Inspector. The understanding of each and every concept used by INENDI Inspector is not mandatory to start using the software but readers of this manual are strongly encouraged to read this chapter before rushing on the application.

Should the reader be in a real hurry, each section of this chapter starts by a very short presentation of the corresponding concept. Our advice is that all readers of this manual should read all these small definitions, at least.

Moreover, the organisation of this chapter is more logical than alphabetical: concepts are presented in such a way that the newly defined concepts only rely on already defined ones. Of course, for some terms, links to terms that are defined later in this chapter are given.

But to ease the use of this chapter as a reference of concepts, we provide the alphabetically ordered list of links to concepts :

* pvkw:AxesCombination[]
* pvkw:Axis[]
* pvkw:Column[]
* pvkw:DataCollection[]
* pvkw:DataTree[]
* pvkw:Event[]
* pvkw:Field[]
* pvkw:Filter[]
* pvkw:Format[]
* pvkw:Input[]
* pvkw:InvalidEvent[]
* pvkw:Investigation[]
* pvkw:Layer[]
* pvkw:LineProperty[]
* pvkw:Mapping[]
* pvkw:Plotting[]
* pvkw:SelectableEvent[]
* pvkw:Selection[]
* pvkw:Source[]
* pvkw:View[]
* pvkw:Workspace[]
* pvkw:ZombieEvent[]
* pvkw:Zone[]




//=============================================================================
[[concept-event]]
Event
~~~~~

An pvkw:Event[] is a well-defined block of information, commonly represented by a line in a file.


An pvkw:Event[] is the basic unit of data that composes a whole set of Data. For example, if one considers a Log file of a web server, if we suppose that each line of this Log file has the same structure then each individual line of this file can be considered as an pvkw:Event[]. +
When data comes from an industrial process, an pvkw:Event[] can be composed of the measurement (say, every minute) of a set of physical sensors, to monitor the state of an industrial plant.

pvkw:Events[] are commonly stored as fixed-sized tuples (such as in a CSV file); however, some appliances or applications use a tree based block structure (such as XML) to store the information.



//=============================================================================
[[concept-input]]
Input
~~~~~

An pvkw:Input[] is a set of pvkw:Events[]: it can be a local file, a remote file, or the result of a database query.

The term pvkw:Input[] is a generic term used to name a dataset that can be processed by INENDI Inspector. Most of the time, it is a plain-text file sitting on a local filesystem. But it might also be such a file accessed on a remote computer or the result of a query sent to a database.

To be able to process an pvkw:Input[], INENDI Inspector needs a descriptive file called a pvkw:Format[] (explained later in the chapter).



//=============================================================================
[[concept-field]]
Field
~~~~~

A pvkw:Field[] is a specific part of an pvkw:Event[] that can be found and located on all the pvkw:Events[]. Usually, a pvkw:Field[] is always located at the same position in the pvkw:Events[].

For example, it often happens that each line of a log file starts by a timestamp. In this case, one can consider that the first pvkw:Field[] of all these pvkw:Events[] is a timestamp.

A login name, a URL or a port number are other examples of classical pvkw:Fields[] that are commonly found in Log files.

Sometimes, according to the analysis' needs, some pvkw:Fields[] found in the pvkw:Events[] need to be further processed and split into simpler pvkw:Fields[]. +
For example:

* an URI can be further split into Scheme, Hostname, Port and Resources;
* a Timestamp can be split into year, month, day, hours, minutes and seconds.

The complete splitting process of an pvkw:Event[] into smaller pvkw:Fields[] is described in the pvkw:Format[] associated to an pvkw:Input[].


//=============================================================================
[[concept-column]]
Column
~~~~~~

A pvkw:Column[] is the natural representation of all the values taken by a given pvkw:Field[] in a given pvkw:Input[], starting with the first pvkw:Event[] in the first row, and ending by the last pvkw:Event[] in the last row of the pvkw:Column[].

This terminology is obvious to all those what keep in mind the tabular representation of the pvkw:Input[]: the pvkw:Events[] correpond to rows, and the pvkw:Fields[] correspond to columns.

In fact, an pvkw:Input[] inherits its tabular representation (as a dataset) after the application of an associated pvkw:Format[].


//=============================================================================
[[concept-axis]]
Axis
~~~~

In INENDI Inspector, an pvkw:Axis[] is the most common mathematical representation of a pvkw:Column[] found in a given pvkw:Input[]. An pvkw:Axis[] is then completely tied to the pvkw:Column[] it represents.

The term pvkw:Axis[] will often come when dealing with graphical representations (called pvkw:Views[] in INENDI Inspector).



//=============================================================================
[[concept-mapping]]
Mapping
~~~~~~~

A pvkw:Mapping[] is the first function (out of two) that is applied to all the values of a pvkw:Column[] so that they can be mapped (i.e. positioned) on the associated mathematical pvkw:Axis[].

INENDI Inspector offers different pvkw:Mapping[] functions. Some are generic and can be applied to any type of values of pvkw:Fields[]. Some are only meaningfull on specific types of pvkw:Fields[].

In fact, a pvkw:Mapping[] holds some more parameters and settings that are usefull for the graphical representations (colors, etc.). But the main purpose of a pvkw:Mapping[] is to map (hopefully!).



//=============================================================================
[[concept-plotting]]
Plotting
~~~~~~~~

A pvkw:Plotting[] is the second function that determines how values contained in pvkw:Fields[] are plotted to a finite portion of a mathematical axis. The main purpose of a pvkw:Plotting[] is to set the scale and range applied to the mathematical values computed by the previous pvkw:Mapping[] operation.



//=============================================================================
[[concept-axes-combination]]
Axes combination
~~~~~~~~~~~~~~~~

An pvkw:AxesCombination[] is a very natural concept in INENDI Inspector. After a given pvkw:Input[] has been processed by INENDI Inspector, a certain number of pvkw:Fields[] are available and form a set of pvkw:Columns[]/pvkw:Axis[]. Out of this set, the user can select all the pvkw:Fields[] it really needs for his or her investigation, and determine in which order these pvkw:Fields[] should be organized.

Of course, through multiple selection, it is possible to repeat the some pvkw:Field[]/pvkw:Column[]/pvkw:Axis[] in the pvkw:AxesCombination[].



//=============================================================================
[[concept-format]]
Format
~~~~~~

A pvkw:Format[] describes how an pvkw:Input[] will be processed and represented in INENDI
Inspector. The pvkw:Format[] gives the logic about how the pvkw:Events[] are split into
pvkw:Fields[]; it gives meta-informations about pvkw:Fields[] too:

* their type: 'IP', 'integer', 'string', and so on;
* their displayed name: 'request', 'response', 'error code', and so on;
* their associated pvkw:Mapping[];
* their associated pvkw:Plotting[];
* and a few more parameters.

Moreover, a pvkw:Format[] provides an pvkw:AxesCombination[].

The association of a specific type of pvkw:Input[] and a pvkw:Format[] is fundamental in INENDI Inspector.



//=============================================================================
[[concept-invalid-event]]
Invalid event
~~~~~~~~~~~~~

As said earlier, a pvkw:Format[] describes how pvkw:Events[] are extracted from an pvkw:Input[] and split in pvkw:Fields[]. Sometimes, it happens that the extraction and splitting process described by a given pvkw:Format[] fails for some pvkw:Events[].

This can happen for different reasons:

- malformed pvkw:Event[] in the original pvkw:Input[]
- mismatch between the splitting process coded in the pvkw:Format[] and some exotic values contained in the pvkw:Event[]
- etc.

In INENDI Inspector, these unrecognized pvkw:Events[] are named pvkw:InvalidEvents[]. Of course, this is relative to the given pvkw:Format[] and is likely to be handeled perfectly by a more suitable pvkw:Format[].



//=============================================================================
[[concept-source]]
Source
~~~~~~

A pvkw:Source[] is another central concept in INENDI Inspector. It corresponds to the association of one or many pvkw:Inputs[] and a fixed pvkw:Format[] that is adapted to the pvkw:Inputs[].

To give a simple example, a set of Proxy files, all of the same type, and a dedicated pvkw:Format[] (that is adapted to this type of Log file) form a pvkw:Source[].


//=============================================================================
[[concept-data-collection]]
Data collection
~~~~~~~~~~~~~~~
A data collection is a convenience to gather different pvkw:Sources[]
according to a free criteria. This criteria can the duration covered by the pvkw:Sources[], the sources types, etc.



//=============================================================================
[[concept-investigation]]
Investigation
~~~~~~~~~~~~~

pvkw:Investigations[] are what INENDI Inspector users will use to save their work and be able to resume their analysis later. It allows them to save:

- a connection to the original data that was used for a particular investigation
- all pvkw:GraphicalViews[] that were in use at the time of saving ^(*)^
- all the pvkw:Layers[] and their states




//=============================================================================
[[concept-layer]]
Layer
~~~~~

A layer is a sub-set of a pvkw:Source[]'s pvkw:Event[] set.



//=============================================================================
[[concept-zombie-event]]
Zombie event
~~~~~~~~~~~~

A zombie event is an pvkw:Event[] which does not belong to the current
pvkw:Layer[].



//=============================================================================
[[concept-selectable-event]]
Selectable event
~~~~~~~~~~~~~~~~

An event is 'selectable' if it belongs to the current layer's event set.



//=============================================================================
[[concept-selection]]
Selection
~~~~~~~~~

A selection is a sub-set of the pvkw:SelectableEvent[] set of the current
pvkw:Layer[].



//=============================================================================
[[concept-zone]]
Zone
~~~~

A zone is a pair of pvkw:Axes[] or of pvkw:Columns[].



//=============================================================================
[[concept-view]]
View
~~~~

A view is the result of the application of the different processes provided by a
pvkw:Format[] on a pvkw:Source[]: extraction,
pvkw:Mappings[], pvkw:Plottings[] and
pvkw:AxesCombination[].



//=============================================================================
[[concept-line-property]]
Line Property
~~~~~~~~~~~~~

A line property is a property set attached to the pvkw:Events[] of a
pvkw:Layer[], such as their colors or their selectability.



//=============================================================================
[[concept-filter]]
Filter
~~~~~~

A pvkw:Filter[] is an algorithm used to alter the pvkw:LinesProperties[] of the current active
pvkw:Layer[] of the pvkw:LayerStackView[].



//=============================================================================
[[concept-workspace]]
Workspace
~~~~~~~~~

A pvkw:Workspace[] is a dockable area containing all the displayed widgets of a pvkw:Source[].



//=============================================================================
[[concept-data-tree]]
Data tree
~~~~~~~~~

A pvkw:DataTree[] is a tree based hierarchical representation of all existing pvkw:Views[] in an pvkw:Investigation[]. It also provides the list of pvkw:DataCollections[], pvkw:Sources[],
pvkw:Mappings[], and pvkw:Plottings[] of the current pvkw:Investigation[].
