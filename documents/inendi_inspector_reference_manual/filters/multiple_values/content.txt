[[ref-multiple-values-search-filter]]
Multiple Values Search Filter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The pvkw:MultipleValuesSearchFilter[] is a text based selection pvkw:Filter[]. It does not change the color of pvkw:Events[]. Its purpose is to select, from the current pvkw:Selection[] of pvkw:Events[], all the pvkw:Events[] that carry a value, on a given pvkw:Axis[], that belongs to a list of patterns provided by the user.

The pvkw:MultipleValuesSearchFilter[] looks like this:

pvimage-600::images/multiple_values.png[]

To use this pvkw:Filter[], there are some parameters and options available to the user. We now present them shortly.

Parameters
^^^^^^^^^^

Case sensitivity
++++++++++++++++

This parameter controls either the match must consider case or not.

pvimage-500::images/multiple_values_param01.png[]

Matching rule
+++++++++++++

This parameter controls either the match considers the whole field must or not.

pvimage-400::images/multiple_values_param02.png[]

Expression type
+++++++++++++++

This parameter controls either the expression is a constant string to match or a regular
expression.

pvimage-500::images/multiple_values_param03.png[]

[CAUTION]
Regular expressions are way slower than plain text because of the inherent cost of regular expressions, but also because fields internal representations can no longer be used for doing the comparisons.

Expression
++++++++++

This parameter controls the expression to use for the search. If the expression contains
more than one line, the result of the search will be the union of the result of the search
using each line.

pvimage-400::images/multiple_values_param04.png[]

Search mode
+++++++++++

This parameter controls either the expression is used to accept events or is used to ignore
them.

pvimage-500::images/multiple_values_param05.png[]

Axis
++++

This parameter enables to choose the considered axis.

pvimage-400::images/multiple_values_param06.png[]

Access
^^^^^^

There are two ways to access this filter.

By using the 'Multiple values...' menu entry:

pvimage-400::images/access_multiple_values_01.png[]

By right-clicking on a cell in the listing view:

pvimage-400::images/access_multiple_values_02.png[]

There are two ways to use this filter.

To run immediatly the search using a value present in the listing, right-click on the
wanted value's cell to open the context menu and select the 'search for this value' action:

pvimage-400::images/fast_access_multiple_values_01.png[]

To open the search dialog using a value present in the listing, right-click on the
wanted value's cell to open the context menu and select the 'search using this value...' action:

pvimage-400::images/fast_access_multiple_values_02.png[]

