[[ref-listing-view]]
Listing View
------------

The purpose of the pvkw:ListingView[] is to present the data in its textual representation. Therefore, it is a tabular representation of the original data with one line of the table dedicated to one pvkw:Event[] of the dataset, and one pvkw:Column[] of the table dedicated to one pvkw:Field[] of the dataset. Each value of a pvkw:Field[] in a given pvkw:Event[] has a textual representation (i.e. as a string) that is shown as a listing in a given cell of a table.

The pvkw:ListingView[] has the following aspect:


pvimage-1200::images/listing_view1.png[]

On the left margin, each row (i.e. line) of the table is referenced by a number which is its row index in the imported dataset. In the upper margin, each column inherits a title that has been defined by the user in the pvkw:Format[] used at import time for the original data.

The bottom part of the pvkw:ListingView[] optionaly shows the pvkw:StatisticsPanel[] associated to the data. This pvkw:StatisticsPanel[] will be described in a later section of this document but we can precise right now that its main purpose is, by default, to show the number of distinct values in each column of the listing.

A line index can be copied to the clipboard by doing a right click on the left margin.

pvimage-200::images/copy_line_index.png[]

By pressing the 'g' key, a dialog allows to go to a given line index. If the line index refers to an unselected pvkw:Event[], the nearest selected one will be used.

pvimage-200::images/goto_line.png[]


Selected, Unselected and Zombie Events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As INENDI Inspector is all about investigating and diving into large datasets of pvkw:Events[], it often happens that one focuses on subsets of the whole dataset. As the investigation goes on, more subsets are created. During the analysis made through INENDI Inspector, it is natural for the user to have some subsets active and some other inactive.

In the terminology of INENDI Inspector, these subsets are associated with pvkw:Layers[]. (A pvkw:Layer[] is a selection of a subset of pvkw:Events[] and, for each of these pvkw:Events[], a color property).

If we consider a specific pvkw:Event[] of the global dataset (the imported pvkw:Source[]), at a given moment in time, it can be in one of the three mutually exclusive states:

- The pvkw:Event[] can be *selected*, meaning that it belongs to one of the active pvkw:Layers[] in the pvkw:LayerStackView[] and it also belongs to the current selection.
- The pvkw:Event[] can be *unselected*, meaning that it belongs to one of the active pvkw:Layers[] in the pvkw:LayerStackView[] but it does not belong to the current selection.
- The pvkw:Event[] can be *zombie*, meaning that it does not belong to any of the active pvkw:Layers[] in the pvkw:LayerStackView[].

This distinction between the different status of an pvkw:Event[] is fundamental. Every user of INENDI Inspector should be comfortable with it.


In the pvkw:ListingView[], the distinction is very easy to observe:

- *Selected* pvkw:Events[] have bright colors (Saturation and Value is at there maximum).
- *Unselected* pvkw:Events[] have shaded colors.
- *Zombie* pvkw:Events[] are in black.

By default, Zombie pvkw:Events[] are not shown in the pvkw:ListingView[].



[[ref-statistics-panel]]
Statistics Panel
~~~~~~~~~~~~~~~~


The pvkw:StatisticsPanel[] located at the bottom of the pvkw:ListingView[] provides a convenient way to quickly access
informations about pvkw:Columns[], such as 'distinct values' or 'sum'.

pvimage-1200::images/listing_view2.png[]

Distinct values dialog visibility can be toggled by clicking on the cell icon.

Right-clicking on the vertical header opens up a contextual menu allowing to chose which tools are enabled or disabled (sum, min, max, average).

pvimage-200::images/stats_panel_tools.png[]

Cell values can be copied to clipboard using the contextual "Copy" menu.

pvimage-200::images/copy_cell_value.png[]

The panel visibility can be toggled by clicking on the "Toggle stats panel visibility" button.

[frame="none",grid="none",valign="bottom"]
|========================
| pvimage-300:images/stats_panel_on.png[] | pvimage-300:images/stats_panel_off.png[]
| stats panel shown | stats panel hidden
|========================

Resizing columns
~~~~~~~~~~~~~~~~

Resizing pvkw:Columns[] to properly fit their content can be done by moving the column header border or
using the 'control'+'mouse wheel' shortcut.

[NOTE]
A cell content that may not fit in the current cell width can nevertheless be fully displayed by leaving the mouse over the cell for a few seconds. The full content pops up as a toolip.


Header context menu
~~~~~~~~~~~~~~~~~~~

The context menu of the listing's header allows to create graphical and statistics pvkw:Views[] on a given
pvkw:Column[] as well as sorting the values of the pvkw:Column[].

.The context menu of the listing's header
pvimage-400::images/header_context_menu.png[]



Cell context menu
~~~~~~~~~~~~~~~~~

The context menu of the listing's cells allows to:

* Apply an pvkw:AxisGradientFilter[] on the axis.
* Filter the values of the column:
 . 'Search using this value...': displays the search dialog filled with the content of the selected cell.
 . 'Search for this value': filters the column using the content of the selected cell.
 . 'Search for': displays an empty search dialog.
* Copy the content of the selected cell to the clipboard.
* Set a color to the selected events.

.The context menu of the listing's cells
pvimage-400::images/cell_context_menu.png[]



Selecting events
~~~~~~~~~~~~~~~~

Selecting pvkw:Events[] from the pvkw:ListingView[] means commiting a selection to the current pvkw:Layer[]. There are
several ways to select pvkw:Events[] from the pvkw:ListingView[]:

. Select a single event by double-clicking on its line number.
. Highlight several events and press Enter.
. Use the contextual menu on a cell to filter events using given criterias.



Visual synchronisation with the parallel view
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Placing the mouse over a column title highlights its associated axis title and helps keeping track
of the association column/axis. +
Note that this highlight works in both ways, thus placing the mouse
over an axis titles highlights its associated column borders.

pvimage-600::images/visual_synchronisation.png[]

Clicking on a column title translates the parallel view so that the associated axis is being placed
on top of it (if the parallel view is located on top of the listing view and are starting at the
same horizontal screen coordinate). Clicking on an axis title translates the listing view so the
associated column becomes visible.



How to access this View
~~~~~~~~~~~~~~~~~~~~~~~

pvimage-300::images/listing_access.png[]
