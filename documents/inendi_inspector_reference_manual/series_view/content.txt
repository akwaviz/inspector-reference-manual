[[ref-series-view]]
Series View
-----------

This visualization displays different sets of metrics evolving over a common dimension.
Each numeric axis of the parallel coordinates view can therefor be displayed as a serie. Thereby, a serie is dependant of the pvkw:Plotting[] of its axis.

pvimage-800::images/series_view.png[]


Zoom
~~~~

Zoom level over the common dimension (X-axis) can be changed using the mouse wheel button or by doing a click-and-drag.
An homothetic zoom can be done using the "Ctrl" modifier both with the mouse wheel button or with a click-and-drag.

It is also possible to zoom between two values using the range widget :

pvimage-400::images/series_view_calendar.png[]

Selection
~~~~~~~~~

Selecting a continuous range of values over the common dimension (X-axis) by doing a "Shift" + click-and-drag will instantly transfert the selection to the others views.

pvimage-800::images/series_view_zoomed.png[]

The current selection of the pvkw:Source[] has also an impact over the display of the timeseries : only the selected values are taken into account an the view is refreshed instantly upon any selection change.

pvimage-500::images/series_view_selection01.png[]
pvimage-500::images/series_view_selection02.png[]

Rendering modes
~~~~~~~~~~~~~~~

Lines
^^^^^

This is the default mode. It allows to easily spot gaps in data.

pvimage-500::images/series_view_lines.png[]

Points
^^^^^^

This alternative mode can be interesting when the values are a bit chaotic.

pvimage-500::images/series_view_points01.png[]

Lines Always
^^^^^^^^^^^^

This mode is advisable when the values are widely spaced and an interpolation is needed. 

pvimage-500::images/series_view_points02.png[]
pvimage-500::images/series_view_lines_always.png[]

Splitting the series according to a specific axis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It can sometimes be convenient to split one or more series according to the values contained in a specific column in order to reveal information that was previously hidden.

The resulting series are then superposed allowing to easily spot abnormal behavior.

pvimage-500::images/series_view_unsplit.png[]
pvimage-500::images/series_view_split.png[]

Any column can be used to split the series by selecting it in the proper combo box :

pvimage-200::images/series_view_unsplit_menu.png[]
pvimage-200::images/series_view_split_menu.png[]

Selecting the series to display
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A click-and-drag over the list of series can toggle activate or deactivate several series at a time.

pvimage-500::images/series_view_list.png[]


Accessing the view
~~~~~~~~~~~~~~~~~~

There are three ways to access this graphic view.

By right-clicking on the header of a zone on the parallel view:

pvimage-400::images/create_series_view_01.png[]

By right-clicking on the header of column in the listing view:

pvimage-500::images/create_series_view_02.png[]

By clicking on the scatter view button located on the workspace toolbar and selection the
appropriate zone:

[frame="none",grid="none",valign="bottom"]
|========================
| pvimage-400:images/create_series_view_03a.png[] | pvimage-400:images/create_series_view_03b.png[]
|========================

The selected axis will then be used a the common dimension (X-axis), and all the other numeric axes will be displayed a series (Y-axis).
