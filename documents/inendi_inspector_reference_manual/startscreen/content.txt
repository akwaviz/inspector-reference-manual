[[ref-start-screen]]
Start screen
------------

When starting INENDI Inspector, it usually looks like this:

pvimage-1200::images/startscreen.png[]



Start Screen organization
~~~~~~~~~~~~~~~~~~~~~~~~~


We observe that the pvkw:StartScreen[] is divided into three areas:

- the pvkw:SourcesArea[]
- the pvkw:FormatsArea[]
- the pvkw:InvestigationsArea[]

In the next subsections, we give a rapid presentation of these three areas. The next sections present each of these areas in detail.

The Sources Area in brief
^^^^^^^^^^^^^^^^^^^^^^^^^

This area gives a short historical list of the most recent pvkw:Sources[] of data that have been imported in the software lately. Use this area and the entries it has, if you want to start a new pvkw:Investigation[] on the designated pvkw:Source[] of data. +
 +
This is the best way to go when one wants to reanalyse a dataset but not reuse an existing pvkw:Investigation[] based on that dataset.

NOTE: to be precise, a pvkw:Source[] is a dataset associated with a pvkw:Format[] that gives INENDI Inspector all the elements it needs to handle and split the pvkw:Events[] of the given dataset. +
It means that if a user clicks on one of the entries of the pvkw:SourcesArea[], it will also ask INENDI Inspector to reuse the same pvkw:Format[] that is associated with this pvkw:Source[] in the historical list.

Needless to say that, when clicking on an entry of the list, the associated dataset will be imported again (the user will have to take into account the duration of the Import Process).




The Formats Area in brief
^^^^^^^^^^^^^^^^^^^^^^^^^

The pvkw:FormatsArea[] is the best place to manage pvkw:Formats[].

From this area, it is very easy to start creating a new pvkw:Format[] or to edit an existing pvkw:Format[]. In the latter case, when one needs to access an already existing pvkw:Format[], the pvkw:FormatsArea[] offers three different short lists of pvkw:Format[]:

- a short historical list of the recently *used* pvkw:Format[]s,
- a short historical list of the recently *edited* pvkw:Format[]s,
- a list of all the pvkw:Formats[] that are distributed and contained in the official INENDI Inspector distribution directories.



The Investigations Area in brief
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

"pvkw:Investigations[]" are INENDI Inspector's native container file format. It is named after the fact that the most common usage of INENDI Inspector is to conduct an investigation and, therefore, when interrupting such a demanding activity, the user usually wants to save as much as it can from the state of the application and the ongoing investigation.

The 'Investigation Area' is the place from where the user will reload a previously saved investigation to continue it.

A simple click on one of the pvkw:Investigation[] of the dedicated historical list will simply reload the targeted pvkw:Investigation[], making it ready to be pursued. (See the pvkw:InvestigationsArea[] section for more details about this topic.)

IMPORTANT: Beware that the loading time of an pvkw:Investigation[] can be of a few minutes, according to the size of the original imported data and the complexity of the pvkw:Layers[]





Managing the history of each area
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


As you have probably already noticed, Recent items are displayed everytime they are relevant in the area. The entries of these historical lists are ordered according to the natural rule: most recent items are located at the top of the list.

pvimage-500::images/per_item_clear_recent_items.png[]

It is possible to remove some items from the history by checking some of them and then clicking on the
'Clear'/'Delete' button.

If you want to remove all the items, just click on the 'Clear'/'Delete' button and confirm that you really want to fully empty the history!


Logs formats
~~~~~~~~~~~~

Most logs that will be investigated through the usage of INENDI Inspector software and Analysis Stations are usually in plain text format. +
In this category, one finds logs files that:

- hold one pvkw:Event[] per line of the file
- are multiline file format: in this case, an pvkw:Event[] has a content that is spread through multiple lines.

On the other side, some logs or datasets are provided in binary formats. Except for a few exceptions, it is most probable that INENDI Inspector won't be able to directly access and process such file formats. +
The binary formats that can be directly opened by INENDI Inspector are:

- compressed files: .zip, .gz, .tgz, etc.
- pcap files.





[[ref-sources-area]]
Sources Area
~~~~~~~~~~~~


[[ref-input-plugins]]
Input plugins
^^^^^^^^^^^^^

We have explained previously that INENDI Inspector processes a set of pvkw:Inputs[] through a chosen pvkw:Format[], to generate pvkw:Events[] split in different pvkw:Fields[]. The types of pvkw:Inputs[] INENDI Inspector can manage is not restricted to plain text files.

INENDI Inspector has a modular architecture that allows it to manage different types of pvkw:Inputs[] through Plugins. The rest of this section will present in detail the three pvkw:InputPlugins[] that are available in the latest version of INENDI Inspector:

- Local files
- Remote files
- Databases



Local files
^^^^^^^^^^^

pvimage-800::images/import_local_files.png[]

There are several ways to specify a pvkw:Format[] to parse input files.

. If the pvkw:Source[] is made of only one input file named 'example.log', 'example.log.format' located in the same folder is used if existing.
. If such a pvkw:Format[] does not exist or if the pvkw:Source[] is an aggregation of several input files, pvkw:Format[] file 'inendi.format' located in the same folder is used if existing.
. If neither of the two previous pvkw:Formats[] are found, pvkw:Formats[] located in the folder '/opt/inendi-inspector/normalize-helpers/text' are used. They can be resolved automatically or be selected explicitely in the 'Treat file as' combo menu located on the options of the file chooser dialog.

[NOTE]
You can make it easier to change the pvkw:Format[] to use by making a 'inendi.format' symbolic link
pointing towards the format of your choice located in any particular folder.

Remote files
^^^^^^^^^^^^

This plugin allows to load files from remote locations using various protocols (HTTP, HTTPS, FTP,
FTPS, SCP, SFTP).

To add a remote file location, click on the 'Add New Machine...' sub-menu located on the "Machine"
menu.

Provide a name for the location as well as its hostname or IP address.

[frame="none",grid="none",valign="bottom"]
|========================
| pvimage-400:images/import_remote_files.png[] |
  pvimage-300:images/import_remote_files_new_machine.png[]
|========================

To add a new remote file, click on the 'Add File...' button and fill in the dialog form.

[frame="none",grid="none",valign="bottom"]
|========================
| pvimage-400:images/import_remote_files_add_file1.png[] |
  pvimage-400:images/import_remote_files_add_file2.png[]
|========================

To load the remote file, double-click on the proper file of the chosen remote machine.

pvimage-500::images/import_remote_files_load_file.png[]

[CAUTION]
Beware that passwords are stored in plain text in '~/.config/inendi*' directories so be sure to
set up the appropriate filesystem permissions.

Databases
^^^^^^^^^

This plugin allows to load result from queries on local/remote databases. Supported databases are:
QIBASE, SQLite, QSQLITE2, QSQLITE3, MySQL, ODBC, QPSQL7, QPSQL, QTDS7, and QTDS.

To set up a database query, select the type of the database, the hostname/IP of the remote machine
and the username/password authorized to connect to the named database.

pvimage-700::images/import_database_connection.png[]

Then type the query that will be executed on the specified database.

pvimage-700::images/import_database_query.png[]

You can check the result of the query on the 'Format' tab by selecting 'Use a format based on this
query using these fields' and clicking 'Update fields'.

pvimage-700::images/import_database_format.png[]

[CAUTION]
Beware that passwords are stored in plain text in '~/.config/inendi*' directories so be sure to
set up the appropriate filesystem permissions.


Elasticsearch
^^^^^^^^^^^^^

This plugin allows to load result from queries on local/remote Elasticsearch server. 

To set up a Elasticsearch query, enter the hostname/IP and the port of the remote machine
and the login/password authorized to connect to Elasticsearch.

pvimage-700::images/import_elasticsearch_connection.png[]

Then select the 'index' and type the query that will be executed on the server. You have three ways to enter the query:

. the graphical Query Builder plugin 
. the Elasticsearch json query language
. the SQL language (You have to install Elasticsearch SQL plugin on the server before - https://github.com/NLPchina/elasticsearch-sql/)

pvimage-700::images/import_elasticsearch_querybuilder.png[]

You can check the result of the query with the 'Get result count' button, export it to CSV file with the 'Export' button, or 'Open' button to investigate it with Inspector.  

pvimage-700::images/import_elasticsearch_buttons.png[]

[NOTE]
Before investigating the query result with Inspector by clicking on the 'Open' button, 
you have to make the suitable Inspector format for this query.

The 'help' tab liste the known limitations of the plugin.

pvimage-700::images/import_elasticsearch_help.png[]

[CAUTION]
Beware that passwords are stored in plain text in '~/.config/inendi*' directories so be sure to
set up the appropriate filesystem permissions.



Splunk
^^^^^^

This plugin allows to load result from queries on local/remote Splunk server. 

To set up a Splunk query, enter the hostname/IP and the port of the remote machine
and the login/password authorized to connect to Splunk.

pvimage-700::images/import_splunk_connection.png[]

Then select the 'index', 'host', 'SourceType' and enter the query that will be executed on the server. You have thwo ways to enter the query:

. the graphical Query Builder plugin 
. the Splunk search API

pvimage-700::images/import_splunk_querybuilder.png[]

You can check the result of the query with the 'Get result count' button, export it to CSV file with the 'Export' button, or 'Open' button to investigate it with Inspector.  

pvimage-700::images/import_splunk_buttons.png[]

[NOTE]
Before investigating the query result with Inspector by clicking on the 'Open' button, 
you have to make the suitable Inspector format for this query.

The 'help' tab liste the known limitations of the plugin.

pvimage-700::images/import_splunk_help.png[]

[CAUTION]
Beware that passwords are stored in plain text in '~/.config/inendi*' directories so be sure to
set up the appropriate filesystem permissions.


Import process
^^^^^^^^^^^^^^

The import is the operation of parsing pvkw:Inputs[] using a given pvkw:Format[] in order to produce an internal
representation that drastically speeds up investigations' interactions.

pvimage-400::images/import_loading.png[]

You can interrupt the operation anytime and chose, either to continue using the data processed so far, or
to discard the processed data completely.


A dialog displaying all the invalid pvkw:Events[] (if any) is shown at the end of the import stage:

pvimage-500::images/invalid_events.png[]

Copying these values to a file can then be useful to refine the format or to create another one.





[[ref-formats-area]]
Formats Area
~~~~~~~~~~~~

Three categories of pvkw:Formats[] are displayed on the pvkw:StartScreen[]:

. Recent used pvkw:Formats[] : list all the pvkw:Formats[] being recently used by the application.
. Recent edited pvkw:Formats[] : list all the pvkw:Formats[] being recently modified.
. Supported pvkw:Formats[] : list all the pvkw:Formats[] located in '/opt/inendi-inspector/normalize-helpers/text'


To learn more about pvkw:Formats[] and how you can write your own pvkw:Formats[] for your logs, go to the pvkw:FormatBuilder[] chapter.


[[ref-investigations-area]]
Investigations Area
~~~~~~~~~~~~~~~~~~~

When saving an investigation, all its underlying structure (data tree) is saved including data
collections with their sources as well as layers defined in these sources.

The import cache is also kept in order to reduce next loading time. Import
cache can then be managed by clicking on the "Delete" button of the "Investigation" section. Note
that if an investigation is deleted using this dialog or directly by removing the investigation
file (.pvi) on disk, the import cache is automatially removed.

To manage investigations, click on the 'Delete' button :

pvimage-400::images/import_cache.png[]

. Clear from history: remove the investigation entry from the history.
. Clear import cache: remove import cache stored on disk (will be recreated on next loading).
. Delete investigation: clear the history, remove the import cache and delete the investigation file (.pvi) on disk.
