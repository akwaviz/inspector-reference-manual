[[ref-views]]
Views in INENDI Inspector
-------------------------

INENDI Inspector is a graphically oriented investigation software on multi-dimensional large sets of data. That's why it uses different graphical representations.

By extension, all widgets that present any kind of data is called a pvkw:View[]. This applies to the following forms of data representations:

- in raw format (as given by the original strings in the text files, for example)
- in a graphical representation
- as the result of computations done on the raw data.


IMPORTANT: In the INENDI Inspector's terminology, these graphical and non-graphical representations are called pvkw:Views[].


The next screenshot gives a sample of the most significant pvkw:Views[] in INENDI Inspector:

pvimage-1200::images/views_small.png[width="1200px"]





[[ref-graphical-views]]
About Graphical Views
~~~~~~~~~~~~~~~~~~~~~

INENDI Inspector provides different pvkw:GraphicalViews[] to help the user interact with its data or get an idea of what this data express.

Some of the pvkw:GraphicalViews[] are directly connected to the data and values it holds. This is the case with the following pvkw:GraphicalViews[]:

- The pvkw:GlobalParallelCoordinatesView[]
- The pvkw:ZoomedParallelCoordinatesView[]
- The pvkw:ScatterPlotView[]

Some of the pvkw:GraphicalViews[] of INENDI Inspector are representations of computations that ran on all or part of the data. This is the case with the following pvkw:GraphicalViews[]:

- The pvkw:HitCountView[]


[[ref-non-graphical-views]]
About Non-Graphical Views
~~~~~~~~~~~~~~~~~~~~~~~~~

As said before, some of the pvkw:Views[] in INENDI Inspector are non-graphical.

Today, this terminology encompasses the following pvkw:Views[]:

- the pvkw:ListingView[]
- the pvkw:LayerStackView[]
- the pvkw:StatisticsViews[]


List of all Views in INENDI Inspector
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


As of the current version of INENDI Inspector, the available pvkw:Views[] are the following:

- the pvkw:ListingView[]
- the pvkw:LayerStackView[]
- the pvkw:GlobalParallelCoordinatesView[]
- the pvkw:ZoomedParallelCoordinatesView[]
- the pvkw:SeriesView[]
- the pvkw:ScatterPlotView[]
- the pvkw:HitCountView[]
- the pvkw:StatisticsViews[]:
* pvkw:DistinctValues[]
* pvkw:CountBy[]
* pvkw:SumBy[]


The aim of the next chapters is to present in detail all the pvkw:Views[] that are available in INENDI Inspector.


