INENDI Inspector Remote Access
==============================

Introduction
------------

During our performance tests, we’ve found Ethernet 100 Megabit being usable but somewhat pretty laggy... As such, for an optimal user experience you should use at least Gigabit Ethernet whenever possible.
By default the INENDI station is configured to automatically launch INENDI Inspector on remote access with X11-Forwarding enabled. The script handling this automatic launch is located at the end of the file «/etc/profile» on the INENDI Station:

****
 if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
         if [ -n "$DISPLAY" ]; then
                 cd /opt/inendi-inspector
                 ./inspector.sh
         fi
 fi
****

From Linux or MacOS X
---------------------

In the case your local machine doesn't have an SSH client installed, you can install it using the following command, depending of your OS.

Debian/Ubuntu/Mint:
****
 # apt-get install openssh-client
****

RHEL/CentOS/Fedora:
****
 # yum -y install openssh-clients
****

Mac OS X: installed by default.


Connect to the remote INENDI station using the following command:
****
 $ ssh -X username@INENDI_STATION_IP # eg: ssh -X inendi@192.168.0.75
****

[NOTE]
Be careful to use an upper case 'X' and not a lower case 'x' otherwise the X11 forwarding would be disabled.
After typing your password, INENDI Inspector is automatically launched on your local machine.


From Windows
------------

In order to remotely access the INENDI station from a Windows session you need to have at your disposal an SSH client as well as an X Window system. You can use any software you like including

http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html[Putty]-http://www.straightrunning.com/XmingNotes/[Xming],

http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html[Putty]-http://sourceforge.net/projects/vcxsrv/[VcXsrv]

or  http://www.cygwin.com/[Cygwin]-http://www.openssh.com/[OpenSSH] 
but we found http://mobaxterm.mobatek.net/download-home-edition.html[MobaXterm] to be the easiest to use.

Once MobaXTerm downloaded and properly installed, configure a remote session to the INENDI station by clicking on the « Session » toolbar button:

pvimage-1000::images/mobaxterm_toolbar_session.png[]

Select the « SSH » tab and specify the IP of the INENDI station as well as the desired username (« inendi » is the default configured session) and save the session.

pvimage-1000::images/mobaxterm_configure_session.png[]

Then click on the « Settings » toolbar button

pvimage-1000::images/mobaxterm_toolbar_settings.png[]

and uncheck the « Enable hardware OpenGL acceleration » located under the « X11 » tab. Hardware OpenGL acceleration needs indeed to be disabled to avoid graphical artifacts.

pvimage-1000::images/mobaxterm_hardware_acceleration.png[]

You are now ready to launch INENDI Inspector from your Windows session by double-clicking on the saved session:

pvimage-1000::images/mobaxterm_start_session.png[]

To disable the automatic launch of INENDI Inspector, click on the « Settings » toolbar button, select « SSH » and « Advanced SSH settings » tabs and uncheck the « X11-Forwarding » checkbox.

pvimage-1000::images/mobaxterm_x11_forwarding.png[]