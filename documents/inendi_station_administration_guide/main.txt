INENDI Station Administration Guide
===================================

The aim of the INENDI Inspector Administration Guide is to provide a quick way to have your INENDI station secured, up-to-date, and fully configured to your needs.


Dual-screen setup
-----------------

Dual-screen support under Linux works better if the screens are plugged on the same graphic card.

As such, since both the screens and the graphic card support 'DisplayPort' and 'DVI' ports, we recommand that you plug the screens as follow:

[frame="none",grid="none",valign="bottom"]
|========================
| image:images/gc_without_cables.png[] | image:images/gc_with_cables.png[]
|========================

Passwords management
--------------------

[WARNING]
INENDI stations come configured with default passwords but we strongly encourage you to change them as soon as possible for obvious security reasons.

Root account
~~~~~~~~~~~~

****
 $ sudo passwd
****

 [sudo] password for inendi:
 Enter new UNIX password: 
 Retype new UNIX password: 
 passwd: password updated successfully


INENDI account
~~~~~~~~~~~~~~

****
 $ passwd
****

 Enter new UNIX password: 
 Retype new UNIX password: 
 passwd: password updated successfully

Hard-disk encryption passphrase
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

****
 # cryptsetup luksChangeKey </dev/device> --slot 0
****
                                                                                                                                                
 Enter passphrase to be changed:                                                                                                                                                    
 Enter new passphrase:                                                                                                                                                              
 Verify passphrase:

[NOTE]
'</dev/device>' is '/dev/md1' for analysis stations and '/dev/sda2' for demo stations.

In case you tried the wrong device, the following error message gets displayed:

****
 Device </dev/device> is not a valid LUKS device.
****

System update
-------------

INENDI stations OS is GNU/Linux Debian and as such the system can be updated like any standard Debian system using the following commands (if properly connected to the Internet or APT-Proxy):

****
 # apt-get update
 # apt-get upgrade
 # apt-get dist-upgrade
****

Packages available in the standard repositories can also be installed as follow:

****
 # apt-get install <package>
****

INENDI Inspector update
-----------------------

If your INENDI station is connected to the Internet, you can update INENDI Inspector software with the following command:

****
 # inendi-update
****

In the opposite case, you can get the URL and the credentials needed to download an encrypted archive by running the following command on the INENDI station:

****
 # inendi-update --geturl
****

****
 [*] Using temporary directory /tmp/inendiv77fp_update...
 
 Credentials needed to manually grab the release:
   Login: username@your-company.com
   Password: 12345678

 url: http://www.inendi.com/release/0123456789abcdef0123456789abcdef01234567/0123456789abcdef0123456789abcdef01234567.tgz

 [*] Removing temporary files...
****

Once the encrypted archive downloaded and copied to the INENDI station, it can be decrypted with the following command:

****
 # inendi-update --decrypt /path/to/0123456789abcdef0123456789abcdef01234567.tgz
****

A decrypted 'inendi-inspector.tar.gz' archive is then created on your current working directory.

You can now update INENDI Inspector using with the following command:

****
 # inendi-update --install ./inendi-inspector.tar.gz
****

Network share
-------------

Unlike demo stations, analysis stations motherboards only support USB 2.0 which maximal theoretical data rate is 480 Mbit/s (60 MB/s) while effective throughput is likely to be around 35 MB/s.

On the other hand, the network controler embedded in the motherboard supports Gigabit Ethernet which maximal theoretical data rate is 1000 Mbit/s (125 MB/s), for an  effective throughput of +110 MB/s.

In order to achieve the best transfert speed, we recommand that you setup a network share such as SSHFS or NFS.
As the whole data stream is encrypted with SSHFS, the server CPU can in some situation be the bottleneck slowing down the transfert.
Therefore, if you get poor transfert speed and your security constaints are in line, you should rather setup a NFS share.

SSHFS share
~~~~~~~~~~~

INENDI station:
****
 # apt-get install sshfs
 # sshfs [user@]host:[dir] mountpoint [options]
****

NFS share
~~~~~~~~~

Log server (if running Debian):
****
 # apt-get install nfs-kernel-server
 # vim /etc/exports:
    </path/to/logs/> <inendi_station_ip>(ro,all_squash,anonuid=1000,anongid=1000,sync)
 # service nfs-kernel-server restart
****

[WARNING]
In case of problem, check files permissions as well as the content of the following files: /etc/hosts.allow and /etc/hosts.deny

INENDI station:
****
 # apt-get install nfs-common
 # mkdir /srv/remote-logs
 # vim /etc/fstab:
    <server_ip>:</path/to/logs/> /srv/remote-logs nfs defaults,auto,user,rsize=8192,wsize=8192,timeo=14,noatime,nodiratime,intr 0 0
 # mount /srv/remote-logs
****


