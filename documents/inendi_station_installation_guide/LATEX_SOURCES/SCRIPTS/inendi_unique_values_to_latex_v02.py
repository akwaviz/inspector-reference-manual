#!/usr/bin/python
# -*- coding: utf-8 -*-



# This file provides methods to convert the CSV-like export of Unique Values
#  (with or without counts) of Picviz into a LaTeX ready-to-use piece of code.

# REMINDER ####################################################################
# The following characters play a special role in LaTeX and are called "special printing characters", or simply "special characters".
#
#                          # $ % & ~ _ ^ \ { }
#



#######################################
### IMPORTS
#######################################

import os
import sys
if __name__ == "__main__":
	### We change the PYTHONPATH so that our local modules are loaded first
	sys.path.append(sys.path[0] + "/../modules/")


import codecs
import shutil
import time
import locale
import csv




###############################################################################
###  test section
###############################################################################



def latexify_string(raw_string):
	output_string = raw_string
	# We start be replacing the \ symbol
	# WARNING We need to escape the \ in the python string
	output_string = output_string.replace("\\", "\\textbackslash ")
	# We replace all other Special Characters
	output_string = output_string.replace("#", "\\#")
	output_string = output_string.replace("$", "\\$")
	output_string = output_string.replace("%", "\\%")
	output_string = output_string.replace("&", "\\&")
	output_string = output_string.replace("~", "\\~")
	output_string = output_string.replace("_", "\\_")
	output_string = output_string.replace("^", "\\^")
	output_string = output_string.replace("{", "\\{")
	output_string = output_string.replace("}", "\\}")
	# For security of formating, we explicitely replace some symbols
	output_string = output_string.replace("|", "\\textbar ")

	return output_string





###############################################################################
#
# convert_pv_statistics_to_latex_longtable
#
###############################################################################
def convert_pv_statistics_to_latex_longtable(input_file, output_file):

	# We need strings to output the LaTeX table
	begin_string = u"""\\begin{center}
\\begin{longtable}{V{0.6\\textwidth} | C | P}
\\caption[Résumé]{Le titre.} \\label{g} \\\\
\\specialrule{1.3pt}{-2pt}{0pt}
% \\bottomrule
% \\hline
\\rowcolor{PVHeaders}
{\\small\\sffamily\\textbf{Value}} & {\\small\\sffamily\\textbf{Counts}} & {\\small\\sffamily\\textbf\\%}
\\\\ \\hline
\\endfirsthead
%
\\multicolumn{3}{c}%
{{\\bfseries \\tablename\\ \\thetable{} -- suite de la page pr\\'ec\\'edente}} \\\\
\\rowcolor{PVHeaders}
{\\small\\sffamily\\textbf{Value}} & {\\small\\sffamily\\textbf{Counts}} & {\\small\\sffamily\\textbf{\\%}}
\\\\ \\hline
\\endhead
\\hline \\multicolumn{3}{r}{{suite en page suivante\\ldots}} \\\\ \\hline
\\endfoot
\\specialrule{1.3pt}{0pt}{0pt}
\\endlastfoot
"""

	end_string = """
\\end{longtable}
\\end{center}
"""
	row_string_template = """\\seqsplit{%(value_s)s} & %(counts_s)s & %(percentage_s)s%(symbol_s)s \\\\ \\hdashline[0.7pt/2pt]
"""

	# We prepare two FileObjects
	input_fo = codecs.open(input_file, encoding='utf-8', mode='r')
	output_fo = codecs.open(output_file, encoding='utf-8', mode='w')

	# We can place the opening part of the table in the outpur file
	output_fo.write(begin_string)

	# We parse all lines of the INPUT file
	csvreader = csv.reader(input_fo, delimiter=',', quotechar='"')
	for line in csvreader:
		
		# We need to LaTeXify the value string
		value = latexify_string(line[0])
		
		# We remove spaces in counts or ","
		# Important : u"\xa0" is the Unicode String for "unbreakable Space"
		#   (this character is used in the Picviz Inspector output...)
		counts = line[1].replace(u"\xa0", "").replace(u",", "") 
		
		# We remove the '%' at the end
		percentage = line[2][:-1] 

		# we forge the LaTeX string for the row
		row_string = row_string_template % {'value_s': value, 'counts_s': counts, 'percentage_s': percentage, 'symbol_s': '\%'}

		# We output this row string
		output_fo.write(row_string)

	# We can now close the table environment
	output_fo.write(end_string)

	# It's time to close the files.
	input_fo.close()
	output_fo.close()








###############################################################################
#
# convert_pv_statistics_to_latex_table
#
###############################################################################
def convert_pv_statistics_to_latex_table(input_file, output_file):

	# We need strings to output the LaTeX table
	begin_string = """\\renewcommand{\\arraystretch}{1.3}
\\begin{table}
\\centering
\\begin{tabular}{V{0.6\\textwidth} | C | P}
\\rowcolor{PVHeaders}
\\specialrule{1.3pt}{0pt}{0pt}
\\mc{1}{\\small\\sffamily\\textbf{Value}}  & \\mc{1}{\\small\\sffamily\\textbf{Counts}} & \\mc{1}{\\small\\sffamily\\textbf\%}
\\\\
\\hline
"""

	end_string = """\\specialrule{1.3pt}{0pt}{0pt}
\\end{tabular}
\\end{table}
"""
	row_string_template = """\\seqsplit{%(value_s)s} & %(counts_s)s & %(percentage_s)s%(symbol_s)s \\\\ \\hdashline[0.7pt/2pt]
"""

	# We prepare two FileObjects
	input_fo = codecs.open(input_file, encoding='utf-8', mode='r')
	output_fo = codecs.open(output_file, encoding='utf-8', mode='w')

	# We can place the opening part of the table in the outpur file
	output_fo.write(begin_string)

	# We parse all lines of the INPUT file
	for line in input_fo:
		L = line[:-1].split(",")
		# WARNING : the "," separator can be contained in the first field
		# We name the fields
		percentage = L[-2][1:] + "." + L[-1][:-2]  # We remove the '%' at the end and change the separator
		counts = L[-3][1:-1]
		value = ",".join(L[:-3])[1:-1]
		# We need to LaTeXify the value string
		value_tex_s = latexify_string(value)

		# We remove spaces in counts
		# Important : u"\xa0" is the Unicode String for "unbreakable Space"
		#   (this character is used in the Picviz Inspector output...)
		counts = counts.replace(u"\xa0", "")

		# we forge the LaTeX string for the row
		row_string = row_string_template % {'value_s': value_tex_s, 'counts_s': counts, 'percentage_s': percentage, 'symbol_s': '\%'}

		# We output this row string
		output_fo.write(row_string)

	# We can now close the table environment
	output_fo.write(end_string)

	# It's time to close the files.
	input_fo.close()
	output_fo.close()






	

# We test this function
input_filename="unique_values_stats_with_count_input.txt"
output_filename="unique_values_stats_with_count_output.txt"

convert_pv_statistics_to_latex_longtable(input_filename, output_filename)









