#!/bin/sh

test $# -ne 1 && echo "usage: `basename $0` root-document" && exit 1

DIR_SRC=`dirname $1`
ABS_DIR_SRC=`readlink -f $DIR_SRC`
DOC_NAME=`basename $DIR_SRC`
DIR_DOC="outputs/$DOC_NAME"

SRC="$DIR_SRC/main.txt `find "$DIR_SRC" -type f -a -name content.txt`"

##############################################################################
# write preliminary rules for a given target
# $1: top-level target
# $2: target file
# $3: target dir

write_rules()
{
	cat << EOF
$1 :: $2

$2 :: $3

$3 :
	@mkdir -p \$@

$2 ::`echo $SRC | xargs -n 1 printf " %s"`
EOF
}

##############################################################################
# write rules to install files/directory for a given target
# $1: top-level target
# $2: target file
# $3: source file
# $4: source
# $5: destination
#
write_rules_install()
{
	cat << EOF
$1 :: \$(INSTALLROOT)/$2

\$(INSTALLROOT)/$2 :: \$(INSTALLROOT)

\$(INSTALLROOT)/$2 :: $3
	@echo " INSTALL \$@"
	@rm -rf \$(INSTALLROOT)/$5
	@cp -RP $4 \$(INSTALLROOT)/$5
EOF
}


##############################################################################
# single HTML rules
#
DIR_SNG="$DIR_DOC/single"

OUT_SNG="$DIR_SNG/index.html"

# document's part
write_rules single "$OUT_SNG" "$DIR_SNG"
printf '\t@echo " SHTML   $<"\n'
if test -d "$ABS_DIR_SRC/images"
then
	printf "\t@rm -rf $DIR_SNG/images\n"
fi
printf "\t@\$(ASCIIDOC) \$(SINGLE_OPT) -o \$@ \$<\n"
#printf "\t@\$(A2X) \$(SINGLE_OPT) --destination-dir $DIR_SNG \$<\n"
#printf "\t@mv $DIR_SNG/main.html \$@\n"
printf "\t@rm -rf $DIR_SNG/style\n"
printf "\t@mkdir -p $DIR_SNG/style\n"
printf "\t@cp -RP \$(MAKEDIR)/style/css $DIR_SNG/style/css\n"
printf "\t@cp -RP \$(MAKEDIR)/style/js $DIR_SNG/style/js\n"
printf "\t@cp -RP \$(MAKEDIR)/style/fonts $DIR_SNG/style/fonts\n"
printf "\t@cp -RP \$(MAKEDIR)/style/icons $DIR_SNG/style/icons\n"
if test -d "$ABS_DIR_SRC/images"
then
	printf "\t@rm -rf $DIR_SNG/images\n"
	printf "\t@cp -RP $DIR_SRC/images $DIR_SNG\n"
fi
echo
echo

# install's part
write_rules_install single_install "$DOC_NAME/index.html" "$OUT_SNG" "$DIR_SNG" "$DOC_NAME"
echo
echo

##############################################################################
# chunked HTML rules
#
DIR_CHN="$DIR_DOC/chunked"

OUT_CHN="$DIR_CHN/main.chunked/index.html"

# document's part
write_rules chunked "$OUT_CHN" "$DIR_CHN"
printf '\t@echo " CHTML   $<"\n'
printf "\t@\$(A2X) \$(CHUNKED_OPT) --destination-dir $DIR_CHN $<\n"
echo
echo

##############################################################################
# PDF rules
#
DIR_PDF="$DIR_DOC/pdf"

OUT_PDF="$DIR_PDF/main.pdf"

# document's part
write_rules pdf "$OUT_PDF" "$DIR_PDF"
printf '\t@echo " PDF     $<"\n'
if test -d "$ABS_DIR_SRC/images"
then
	printf "\t@rm -f $DIR_PDF/images\n"
	printf "\t@ln -s $ABS_DIR_SRC/images $DIR_PDF\n"
fi
# *do not* remove "--destination-dir $DIR_PDF": it does its job but it rises a warning
printf "\t@\$(A2X) \$(PDF_OPT) --destination-dir $DIR_PDF \$<\n"
if test -d "$ABS_DIR_SRC/images"
then
	printf "\t@rm -f $DIR_PDF/images\n"
fi
echo

# install's part
write_rules_install pdf_install "$DOC_NAME.pdf" "$OUT_PDF" "$OUT_PDF" "$DOC_NAME.pdf"
echo
