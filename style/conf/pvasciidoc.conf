#
# We define INENDI specific quote characters
#

:include-dir: 'images/'



# The first one is for INENDI Inspector's concepts and keywords
[quotes]
-=pvemphasis


[tags]
pvemphasis=<em class="pvkeyword">|</em>


[macros]
#------------------------------------------------------------------------------
# Inline macros
#------------------------------------------------------------------------------
# Our Image Macro
(?su)(?<!\w)[\\]?(?P<name>pvimage-100):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-200):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-300):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-400):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-500):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-600):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-700):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-800):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-900):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-1000):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=
(?su)(?<!\w)[\\]?(?P<name>pvimage-1200):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=


# Our Keyword Macro
(?su)(?<!\w)[\\]?(?P<name>pvkw):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=

# This is for our testing purposes
(?su)(?<!\w)[\\]?(?P<name>pvtest):(?P<target>\S*?)\[(?P<attrlist>.*?)(?<!\\)\]=

# We try to master the LATEX PNG generation
(?su)[\\]?(?P<name>pvlatex):(?P<subslist>\S*?)\[(?P<passtext>.*?)(?<!\\)\]=[]




#------------------------------------------------------------------------------
# Block macros
#------------------------------------------------------------------------------
# Macros using default syntax.
(?u)^(?P<name>pvimage-large)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-100)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-200)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-300)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-400)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-500)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-600)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-700)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-800)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-900)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-1000)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#
(?u)^(?P<name>pvimage-1200)::(?P<target>\S*?)(\[(?P<attrlist>.*?)\])$=#


# We try to master the LATEX PNG generation
(?u)^(?P<name>pvlatex)::(?P<subslist>\S*?)(\[(?P<passtext>.*?)\])$=#[]


#------------------------------------------------------------------------------
# Markup templates
#------------------------------------------------------------------------------

[pvimage-inlinemacro]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]


[pvimage-100-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="2cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-100-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="2cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-200-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="4cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-200-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="4cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-300-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="6cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-300-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="6cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]


[pvimage-400-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="8cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-400-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="8cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-500-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="10cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-500-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="10cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]


[pvimage-600-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="11cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-600-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="11cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-700-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="11.5cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-700-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="11.5cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-800-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="12cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-800-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="12cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-900-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="13cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-900-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="13cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-1000-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="14cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-1000-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="14cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]



[pvimage-1200-inlinemacro]
ifdef::basebackend-html[]
<span class="image{role? {role}}">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}{title? title="{title}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</span>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<inlinemediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" width="15cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</inlinemediaobject>
endif::basebackend-docbook[]

[pvimage-1200-blockmacro]
ifdef::basebackend-html[]
<div class="imageblock{style? {style}}{role? {role}}{unbreakable-option? unbreakable}"{id? id="{id}"}{align? style="text-align:{align};"}{float? style="float:{float};"}>
<div class="content">
<a class="image" href="{link}">
{data-uri%}<img src="{imagesdir=}{imagesdir?/}{target}" alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"} />
{data-uri#}<img alt="{alt={target}}"{width? width="{width}"}{height? height="{height}"}
{data-uri#}{sys:"{python}" -u -c "import mimetypes,base64,sys; print 'src=\"data:'+mimetypes.guess_type(r'{target}')[0]+';base64,'; base64.encode(sys.stdin,sys.stdout)" < "{eval:os.path.join(r"{indir={outdir}}",r"{imagesdir=}",r"{target}")}"}" />
{link#}</a>
</div>
<div class="title">{caption={figure-caption} {counter:figure-number}. }{title}</div>
</div>
endif::basebackend-html[]
ifdef::basebackend-docbook[]
<figure><title>{title}</title>
{title%}<informalfigure>
<mediaobject>
  <imageobject>
  <imagedata fileref="{imagesdir=}{imagesdir?/}{target}" align="center" width="15cm"/>
  </imageobject>
  <textobject><phrase>{alt={target}}</phrase></textobject>
</mediaobject>
{title#}</figure>
{title%}</informalfigure>
endif::basebackend-docbook[]




[pvkw-inlinemacro]
# A
{set:ref-name:{target@Axes:concept-axis}{target@AxesCombination:concept-axes-combination}{target@Axis:concept-axis}{target@AxisGradientFilter:ref-axis-gradient-filter}}
# C
{eval:'{ref-name}'==''}{set:ref-name:{target@Column:concept-column}{target@Columns:concept-column}{target@CountBy:ref-count-by}{target@DataCollection:concept-data-collection}{target@DataCollections:concept-data-collection}{target@DataTree:concept-data-tree}{target@DataTrees:concept-data-tree}{target@Event:concept-event}{target@Events:concept-event}{target@ExportStatistics:ref-export-statistics}}
# F
{eval:'{ref-name}'==''}{set:ref-name:{target@Field:concept-field}{target@Fields:concept-field}{target@Filter:concept-filter}{target@Filters:concept-filter}{target@Format:concept-format}{target@Formats:concept-format}{target@FormatsArea:ref-formats-area}{target@FormatBuilder:ref-format-builder}{target@FrequencyGradientFilter:ref-frequency-gradient-filter}}
# G
{eval:'{ref-name}'==''}{set:ref-name:{target@GlobalParallelCoordinatesView:ref-global-parallel-coordinates-view}{target@GlobalParallelCoordinatesViews:ref-global-parallel-coordinates-view}{target@GraphicalView:ref-graphical-views}{target@GraphicalViews:ref-graphical-views}{target@HitCountView:ref-hit-count-view}}
# I
{eval:'{ref-name}'==''}{set:ref-name:{target@ImportsArea:ref-imports-area}{target@InvalidEvent:concept-invalid-event}{target@InvalidEvents:concept-invalid-event}{target@Investigation:concept-investigation}{target@Investigations:concept-investigation}{target@InvestigationsArea:ref-investigations-area}{target@Input:concept-input}{target@Inputs:concept-input}{target@InputPlugin:ref-input-plugins}{target@InputPlugins:ref-input-plugins}}
# L
{eval:'{ref-name}'==''}{set:ref-name:{target@Layer:concept-layer}{target@Layers:concept-layer}{target@LayerStack:ref-layer-stack}{target@LayerStackView:ref-layer-stack-view}{target@LayerStackViews:ref-layer-stack-view}{target@LineProperty:concept-line-property}{target@LineProperties:concept-line-property}{target@LinesProperties:concept-line-property}{target@ListingView:ref-listing-view}}
# M
{eval:'{ref-name}'==''}{set:ref-name:{target@Mapping:concept-mapping}{target@Mappings:concept-mapping}{target@MultipleValuesSearchFilter:ref-multiple-values-search-filter}{target@Plotting:concept-plotting}{target@Plottings:concept-plotting}{target@Preset:ref-presets}{target@Presets:ref-presets}{target@PVRM:PVRM-start}}
# S
{eval:'{ref-name}'==''}{set:ref-name:{target@ScatterPlotView:ref-scatter-plot-view}{target@ScatterPlotViews:ref-scatter-plot-view}{target@SeriesView:ref-series-view}{target@SelectableEvent:concept-selectable-event}{target@SelectableEvents:concept-selectable-event}{target@Selection:concept-selection}{target@Selections:concept-selection}{target@Source:concept-source}{target@Sources:concept-source}{target@SourcesArea:ref-sources-area}{target@StartScreen:ref-start-screen}{target@StatisticsPanel:ref-statistics-panel}{target@StatisticsViews:ref-statistics-views}{target@SumBy:ref-sum-by}}
# U
{eval:'{ref-name}'==''}{set:ref-name:{target@DistinctValues:ref-distinct-values}}
# V
{eval:'{ref-name}'==''}{set:ref-name:{target@View:concept-view}{target@Views:concept-view}{target@Workspace:concept-workspace}{target@Workspaces:concept-workspace}{target@ZombieEvent:concept-zombie-event}{target@ZombieEvents:concept-zombie-event}{target@Zone:concept-zone}{target@Zones:concept-zone}{target@ZoomedParallelCoordinatesView:ref-zoomed-parallel-coordinates-view}{target@ZoomedParallelCoordinatesViews:ref-zoomed-parallel-coordinates-view}}
##
# A
{set:link-name:{target@Axes:Axes}{target@AxesCombination:Axes Combination}{target@Axis:Axis}{target@AxisGradientFilter:Axis Gradient Filter}}
# C
{eval:'{link-name}'==''}{set:link-name:{target@Column:Column}{target@Columns:Columns}{target@CountBy:Count By}{target@DataCollection:Data Collection}{target@DataCollections:Data Collections}{target@DataTree:Data Tree}{target@DataTrees:Data Trees}}
# E
{eval:'{link-name}'==''}{set:link-name:{target@Event:Event}{target@Events:Events}{target@ExportStatistics:Exporting statistics}{target@Field:Field}{target@Fields:Fields}{target@Filter:Filter}{target@Filters:Filters}{target@Format:Format}{target@Formats:Formats}{target@FormatsArea:Formats Area}{target@FrequencyGradientFilter:Frequency Gradient Filter}{target@GlobalParallelCoordinatesView:Global Parallel Coordinates View}{target@FormatBuilder:Format Builder}}
# G
{eval:'{link-name}'==''}{set:link-name:{target@GlobalParallelCoordinatesViews:Global Parallel Coordinates Views}{target@GraphicalView:Graphical View}{target@GraphicalViews:Graphical Views}{target@HitCountView:Hit Count View}}
# I
{eval:'{link-name}'==''}{set:link-name:{target@ImportsArea:Imports Area}{target@InvalidEvent:Invalid Event}{target@InvalidEvents:Invalid Events}{target@Investigation:Investigation}{target@Investigations:Investigations}{target@InvestigationsArea:Investigations Area}{target@Input:Input}{target@Inputs:Inputs}{target@InputPlugin:Input Plugin}{target@InputPlugins:Input Plugins}}
# L
{eval:'{link-name}'==''}{set:link-name:{target@Layer:Layer}{target@Layers:Layers}{target@LayerStack:Layer Stack}{target@LayerStackView:Layer Stack View}{target@LayerStackViews:Layer Stack Views}{target@LineProperty:Line Property}{target@LineProperties:Line Properties}{target@LinesProperties:Lines Properties}{target@ListingView:Listing View}{target@Mapping:Mapping}{target@Mappings:Mappings}{target@MultipleValuesSearchFilter:Multiple Values Search Filter}}
# P
{eval:'{link-name}'==''}{set:link-name:{target@Plotting:Plotting}{target@Plottings:Plottings}{target@PVRM:INENDI Inspector Reference Manual}{target@Preset:Preset}{target@Presets:Presets}}
# S
{eval:'{link-name}'==''}{set:link-name:{target@ScatterPlotView:Scatter Plot View}{target@ScatterPlotViews:Scatter Plot Views}{target@SeriesView:Series View}{target@SelectableEvent:Selectable Event}{target@SelectableEvents:Selectable Events}{target@Selection:Selection}{target@Selections:Selections}{target@Source:Source}{target@Sources:Sources}{target@SourcesArea:Sources Area}{target@StartScreen:Start Screen}{target@StatisticsPanel:Statistics Panel}{target@StatisticsViews:Statistics Views}{target@SumBy:Sum By}}
# U
{eval:'{link-name}'==''}{set:link-name:{target@DistinctValues:Distinct Values}}
# V
{eval:'{link-name}'==''}{set:link-name:{target@View:View}{target@Views:Views}{target@Workspace:Workspace}{target@Workspaces:Workspaces}{target@ZombieEvent:Zombie Event}{target@ZombieEvents:Zombie Events}{target@Zone:Zone}{target@Zones:Zones}{target@ZoomedParallelCoordinatesView:Zoomed Parallel Coordinates View}{target@ZoomedParallelCoordinatesViews:Zoomed Parallel Coordinates Views}}
ifdef::basebackend-html[]
<span class="pvkeyword"><a href="#{ref-name}">{link-name}</a></span>
endif::basebackend-html[]
ifdef::basebackend-latex[]
$\sum$
endif::basebackend-latex[]
ifdef::basebackend-docbook[]
<phrase role="pvkeyword"><link linkend="{ref-name}">{link-name}</link></phrase>
{link-name%}<xref linkend="{ref-name}">
endif::basebackend-docbook[]



[pvtest-inlinemacro]



[blockdef-pvnulllatex]
delimiter=^p{6,}$
template=listingblock
filter='latex2png.py -m{verbose? -v}{dpi? -D {dpi}} -o "images/{target}" -'


# This is an attempt to generate nice LATEX rendered inline PNG in the HTML and PDF output
# This is ??? backend independent ???
[pvlatex-inlinemacro]
# Synthesize missing target attribute for filter generated file names.
# The tag split | ensures missing target file names are auto-generated
# before the filter is executed, the remainder (the [image-blockmacro])
# is excuted after the filter to ensure data URI encoding comes after
# the image is created.
{target%}{counter2:target-number}
{target%}{set2:target:{docname}__{target-number}.png}
{set2:local-text:{passtext}}
{set2:imagesdir:images/tex}
template::[image-inlinemacro]
template::[pvnulllatex]
pppppp
{passtext}
pppppp


[paradef-inbrief]
delimiter=(?s)^\s*(?P<style>INBRIEF):\s+(?P<text>.+)
template::[paragraph-styles]



[paragraph-styles]
INBRIEF-style=template="admonitionparagraph",name="inbrief",caption="In Brief"


# This is an attempt to generate nice LATEX rendered inline PNG in the HTML and PDF output
# This is ??? backend independent ???
[pv-filter-image-inlinemacro]
# Synthesize missing target attribute for filter generated file names.
# The tag split | ensures missing target file names are auto-generated
# before the filter is executed, the remainder (the [image-blockmacro])
# is excuted after the filter to ensure data URI encoding comes after
# the image is created.
{target%}{counter2:target-number}
{target%}{set2:target:{docname}__{target-number}.png}
|
template::[image-inlinemacro]
